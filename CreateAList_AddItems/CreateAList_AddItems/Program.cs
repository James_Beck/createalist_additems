﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CreateAList_AddItems
{
    class Program
    {
        static void Main(string[] args)
        {
            //Store of variables
            List<string> colours = new List<string>();
            colours.Add("red");
            colours.Add("blue");
            colours.Add("orange");
            colours.Add("black");
            colours.Add("white");
            string name;

            //Explation of the program
            Console.WriteLine("Hi there, what's your name?");
            name = Console.ReadLine();
            Console.WriteLine("Nice to meet you {0}. You're in a program tha prints the values of stored colours from a list called <colours>.", name);
            Console.WriteLine();
            foreach (string i in colours)
            {
                Console.WriteLine(i);
            }
        }
    }
}
